from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait

def test_google_search():
    driver = WebDriver (executable_path='D://QA//Automation//selenium//chromedriver.exe')
    driver.get('https://google.com')
    search_input = driver.find_element_by_xpath('//input[@type="text"]')
    search_button = driver.find_element_by_xpath('//div//input[@type="submit"]')
    search_input.send_keys('yahoo')
    search_button.submit()

    def check_results_count(driver):
        inner_search_results = driver.find_element_by_xpath('//div[@class="g"]')
        return len (inner_search_results) >= 5

    WebDriverWait(driver, 4, 0.5).until(check_results_count, 'Results quantity less than 5')

    search_results = driver.find_element_by_xpath ('//div[@class="g"]')
    link = search_results[0].find_element_by_xpath ('.//div[@class="r"]/a')
    link.click()

    driver.switch_to.window(driver.window_handles[1])

    assert driver.title == 'Yahoo'
